module.exports = {
  PORT: 4005,
  ENV: '"production"',
  NODE_ENV: '"production"',
  DEBUG_MODE: false,
  BASE_URL_API: '"https://hrapi.nexlab.vn"',
  HOST: '"http://localhost:4002"'
};
