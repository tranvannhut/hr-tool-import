module.exports = {
  PORT: 4005,
  ENV: '"development"',
  NODE_ENV: '"development"',
  DEBUG_MODE: true,
  // BASE_URL_API: '"http://localhost:5000"',
  BASE_URL_API: '"https://hrapi.nexlab.vn"',
  HOST: '"http://localhost:4002"'
};
