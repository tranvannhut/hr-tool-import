// tslint:disable:no-console
import * as webpack from 'webpack';
import webpackConfig from '../webpack.config';
// Build webpack
webpack(webpackConfig(process.env.NODE_ENV as string, process.argv[2])).run((err: Error, stats: webpack.Stats) => {
    if (err) {
        throw err;
    }
    console.log(stats.toString({
        colors: true,
    }));
});
