function loadScript(url, callback) {
  const head = document.head;
  const script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = url;
  script.onreadystatechange = callback;
  script.onload = callback;
  head.appendChild(script);
}
loadScript('http://localhost:8000/main.js');