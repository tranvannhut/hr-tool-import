import * as path from 'path';
import {
    Entry, DefinePlugin, Plugin, ProvidePlugin
} from 'webpack';
import * as UglifyJsPlugin from 'uglifyjs-webpack-plugin';

export default (env: string, version = 'development'): any => {
    const environment = require(`./environment/${env || 'development'}.env`);
    const isProduction = env === 'production';
    const entry: Entry = {
        main: './src/index.ts'
    };
    const devtool: 'source-map' | 'eval-source-map' | boolean = isProduction
        ? 'source-map'
        : 'eval-source-map';
    const plugins: Plugin[] = [
        new DefinePlugin({
            'process.env': {
                ...environment,
                VERSION: typeof version === 'object' ? `"development"` : `"${version}"`
            }
        }),
        new ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ];
    return {
        entry,
        output: {
            filename: isProduction ? '[name].js' : '[name].js',
            path: path.resolve(__dirname, 'dist', 'static'),
            publicPath: '/',
        },
        node: {
            __dirname: false
        },
        devtool,
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.css'],
            alias: {
                app: path.resolve(__dirname, 'src', 'app'),
                assets: path.resolve(__dirname, 'src', 'assets'),
            }
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: ['awesome-typescript-loader'],
                    exclude: /node_modules/,
                }
            ]
        },
        plugins,
        mode: isProduction ? 'production' : 'development',
        optimization: isProduction ? {
            minimizer: [new UglifyJsPlugin({
                sourceMap: true
            })]
        } : {},
        devServer: !isProduction ? {
            // contentBase: path.join(__dirname, 'dist'),
            compress: true,
            disableHostCheck: true,
            port: 8000,
            contentBase: './src',
            open: true,
        } : {}
    };
};
