import { styles, templateLogin, templatePopulate, TypeStep } from './templates';
import {
  login, getCurrentUser, logout, keyCacheProfile,
  getProfiles, populateData, KeyStore, syncData, TypeSync
  // , clearCurrentForm
} from './controllers';
import { getDataFromStore } from './controllers/store/action';
import { getNamePermission, removeAccent } from './utils';

async function getHtmlTemplate(user: any) {
  return `
  <div class="header">
    <div class="left">
      <b>Import tool</b>
      ${user ? '<button id="btn-refresh" class="blue-background">Refresh</button>' : ''}
    </div>
    ${user ? `
    <div class="right">
      <span>Hello ${user.fullName}, ${getNamePermission(user.role)}</span></br>
      <span>Email: ${user.email}</span>
    </div>`
      : ''}
  </div>
  <div class= "body-tool">
  ${ user ? await templatePopulate() : templateLogin}
  </div>
`;
}

async function reRender(user) {
  $('.main-tool').html(await getHtmlTemplate(user));
}

export const keyCacheSelectedProfile = 'profile_selected';

$(document).ready(async (e) => {
  localStorage.removeItem(keyCacheProfile);
  let populated = false;

  let user = await getCurrentUser();
  $('html').prepend(`
    ${styles}
    <div class="main-tool">
    ${await getHtmlTemplate(user)}
    </div>
    `);
  const profileSelected = localStorage.getItem(keyCacheSelectedProfile);
  if (profileSelected) {
    $('#profiles').val(localStorage.getItem(keyCacheSelectedProfile));
  }

  if (getDataFromStore(KeyStore.IsAutoPopulate)) {
    const currentStep = getDataFromStore(KeyStore.KeyCurrentStep);
    if (currentStep) {
      switch (removeAccent(currentStep)) {
        case removeAccent(TypeStep.Allowance):
          const currentProfile = (await getProfiles()).find(e => e.email === $('#profiles').val());
          if (currentProfile) {
            await populateData(currentProfile.profileOfUser);
          }
          break;
        default:
          break;
      }
    }

  }

  // listen event
  $('html')
    .on('change', '#profiles', async function() {
      $('#selected-profile').text((await getCurrentProfile()).email);
      localStorage.setItem(keyCacheSelectedProfile, $(this).val().toString());
      if (populated) {
        window.location.reload();
      }
      // await clearCurrentForm();
      // window.scrollTo(0, 0);
    })
    .on('click', '#btn-login', () => {
      const username = $('#username').val() as string;
      const password = $('#password').val() as string;
      if (username && password) {
        login(username, password).then(data => reRender(data));
      }
    })
    .on('click', '#btn-logout', () => {
      logout();
      reRender(null);
    })
    .on('click', '#btn-refresh', async () => {
      localStorage.removeItem(keyCacheProfile);
      user = user || await getCurrentUser();
      reRender(user);
      window.location.reload();
    })
    .on('click', '#btn-populate', async () => {
      const button = $('#btn-populate');
      const loaded = () => {
        button.removeAttr('disabled');
        button.text('Populate');
        button.removeClass('color_loading');
      };
      button.attr('disabled', 'disabled');
      button.text('Loading...');
      button.addClass('color_loading');
      try {
        const currentProfile = await getCurrentProfile();
        if (currentProfile) {
          await populateData(currentProfile.profileOfUser);
        }
        populated = true;
        loaded();
      } catch (error) {
        alert(error);
        populated = true;
        loaded();
      }
    })
    .on('click', `#btn-sync_${TypeSync.StaffId}`, async function() {
      await syncData($(this).attr('key'), $(this), await getCurrentProfile());
    })
    .on('click', `#btn-sync_${TypeSync.DecisionNumber}`, async function() {
      await syncData($(this).attr('key'), $(this), await getCurrentProfile());
    })
    .on('click', `#btn-sync_${TypeSync.DoneHris}`, async function() {
      await syncData($(this).attr('key'), $(this), await getCurrentProfile());
    })
    .on('click', `#btn-sync_${TypeSync.ContractNo}`, async function() {
      await syncData($(this).attr('key'), $(this), await getCurrentProfile());
    });

});

export async function getCurrentProfile() {
  return (await getProfiles()).find(e => e.id === $('#profiles').val());
}
