import { TypeInput, IRadio, IForm } from '../../form';
import { inputNormal, inputDropdown, checkBox, radio } from '.';

export async function populateCommon(formData: IForm[], current, clearData = false) {
  return new Promise((resolve, reject) => {
    const arr = formData.map(e => {
      return {
        ...e,
        value: clearData ? '' : current[e.key],
      };
    }).filter(e => e).filter(e => ![null, undefined, ''].includes(e.value));

    const runFbyAwait = async (index) => {
      try {
        if (index > arr.length - 1) {
          return resolve(true);
        }
        const currentData = arr[index];
        if (currentData.parseValue) {
          currentData.value = currentData.parseValue(currentData.value, current);
        }
        console.log('Working on ' + currentData.name);
        switch (currentData.type) {
          case TypeInput.Dropdown:
            if (process.env.NODE_ENV === 'development') {
              inputNormal(currentData.input, currentData.value);
            } else {
              await inputDropdown(
                currentData.input,
                currentData.value, currentData.name, clearData
              );
            }
            break;
          case TypeInput.CheckBox:
            checkBox(currentData.input, currentData.value, clearData);
            break;
          case TypeInput.Radio:
            radio(currentData.input as IRadio[], currentData.value, clearData);
            break;
          default:
            inputNormal(currentData.input, currentData.value);
        }

        return runFbyAwait(++index);
      } catch (error) {
        return reject(error);
      }
    };

    return runFbyAwait(0);
  });
}
