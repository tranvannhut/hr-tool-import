import { populateCommon } from './common';
import { configFormAllowance } from '../../form/form-allowance';
import { getDataFromStore, setDataToStore, removeDataFromStore } from '../../controllers/store/action';
import { KeyStore } from './types';
import { TypeStep } from '../../templates';

export async function populateAllowance(current, clearData = false) {
  try {
    const bonuses = current.bonuses;
    if (bonuses.length === 0) { return true; }
    try {
      setDataToStore(KeyStore.IsAutoPopulate, true);
      setDataToStore(KeyStore.IsShowPopulate, false);
      setDataToStore(KeyStore.KeyCurrentStep, TypeStep.Allowance);
      const keyCurrent = getDataFromStore(KeyStore.KeyCurrentIndexAllowance) || 0;
      setDataToStore(KeyStore.KeyCurrentIndexAllowance, keyCurrent + 1);
      await populateCommon(configFormAllowance, bonuses[keyCurrent], clearData);
      if (keyCurrent === bonuses.length - 1) {
        removeDataFromStore([
          KeyStore.KeyCurrentStep, KeyStore.KeyCurrentIndexAllowance,
          KeyStore.IsAutoPopulate, KeyStore.IsShowPopulate
        ]);
      }
      return document.getElementById('_ctl0_btnSave').click();
    } catch (error) {
      return Promise.reject(error);
    }
  } catch (error) {
    return Promise.reject(error);
  }
}
