import { populateCommon } from './common';
import { configFormSalary } from '../../form';

export async function populateSalary(current, clearData = false) {
  try {
    await populateCommon(configFormSalary, current, clearData);
    return document.getElementById('_ctl0_btnCalc').click();
  } catch (error) {
    return Promise.reject(error);
  }
}
