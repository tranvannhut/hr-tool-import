import { httpClient } from '../base-service';
import {
  configFormAddStaff, configFormWorkProgress,
  configFormLaborContract, IRadio, configFormSalary
} from '../../form';
import { getStep, TypeStep } from '../../templates';
import { removeAccent } from '../../utils';
import { populateCommon } from './common';
import { populateAllowance } from './allowance';
import { populateSalary } from './salary';

export const keyCacheProfile = 'profile';
const urlProfile = `${process.env.BASE_URL_API}/users/profiles`;
const TimeOutInputDropDown = 0;

export function getProfiles() {
  const cache = localStorage.getItem(keyCacheProfile);
  if (cache) { return JSON.parse(cache); }
  return httpClient({
    method: 'GET',
    url: urlProfile
  }).then(resp => {
    const data = resp.data;
    localStorage.setItem(keyCacheProfile, JSON.stringify(data));
    return data;
  });
}

export function eventFire(el, etype) {
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    const evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}

export function waitDropdownLoaded(idInput) {
  return new Promise((resolve, reject) => {
    const idTarget = `${idInput}_DropDown`;
    const qTarget = `#Form1 .rcbSlide #${idTarget} li`;
    const textLoading = 'Loading...';
    if ($(qTarget).length > 1 && $(qTarget).text() !== textLoading) {
      return resolve(true);
    }
    return $('form').bind('DOMSubtreeModified', function() {
      if ($(qTarget).text() !== textLoading) {
        $('form').off('DOMSubtreeModified');
        console.log('Handle dropdown loading');
        return setTimeout(() => resolve(true), TimeOutInputDropDown);
      }
    });
  });
}

export function handleSetData(idInput, val) {
  return new Promise((resolve, reject) => {
    val = val || '';
    const idTarget = `${idInput}_DropDown`;
    const qTarget = `#Form1 .rcbSlide #${idTarget} li`;
    const textLoading = 'Loading...';
    if ($(qTarget).length > 1 && $(qTarget).text() !== textLoading) {
      return resolve(setDataDropdown(qTarget, val));
    }
    return $('form').bind('DOMSubtreeModified', function() {
      if ($(qTarget).text() !== textLoading) {
        $('form').off('DOMSubtreeModified');
        console.log('Handle dropdown loading');
        return setTimeout(() => resolve(setDataDropdown(qTarget, val)), TimeOutInputDropDown);
      }
    });
  });
}

export function setDataDropdown(qEls, val) {
  let setSuccessfully = false;
  $(qEls).each(function() {
    if ($(this).text().toString().trim() === val.toString().trim()) {
      setSuccessfully = true;
      $(this).click();
    }
  });
  return setSuccessfully;
}

export async function populateData(current, clearData = false) {
  switch (removeAccent(getStep())) {
    case removeAccent(TypeStep.Allowance):
      return populateAllowance(current, clearData);
    case removeAccent(TypeStep.Salary):
      return populateSalary(current, clearData);
    default:
      return populateCommon(getConfigForm(), current, clearData);
  }
}

export async function changeInputForFilterDropDown(id, value) {
  const inp: any = document.getElementById(id);
  const ev = new Event('change');

  inp.value = value;
  inp.dispatchEvent(ev);
}

export async function inputDropdown(id, val, inputName, clearData = false) {
  // const idInput = id;
  id = id.replace(':', '_');
  if (!clearData) {
    changeInputForFilterDropDown(`${id}_Input`, val);
  }
  eventFire($(`#${id}_Input`)[0], 'focus');
  eventFire($(`#${id}_Arrow`)[0], 'click');

  /* tslint:disable */
  console.log('Handle input ' + id + ' value: ' + val);
  if (await handleSetData(id, val)) {
    console.log('Set data successfully');
  } else {
    console.error('Unset data');
    if (!clearData) {
      return Promise.reject(inputName + " unset data " + val);
    }
  }
  eventFire($('body')[0], 'click');
  // await inputDropdown(id, val);
  console.log('\n');
  return;
}

export function checkBox(id, val, clearData) {
  const el = `#${id}`;
  if (clearData && $(el).is(":checked")) {
    return $(`#${id}`).click();
  }

  if (val) {
    return $(`#${id}`).click();
  }
  return null
}

export function radio(input: IRadio[], val, clearData) {
  if (clearData) return $(`#${input[0].input}`).click();
  const id = input.find(e => e.value === val).input;
  return $(`#${id}`).click();
}

export function inputNormal(id, val) {
  return $(`input[name='${id}']`).val(val);
}

export async function timeOutP(callback, timeout = 1000) {
  return new Promise((resolve, reject) => {
    callback();
    return setTimeout(() => { resolve(); }, timeout);
  });
}

export function getConfigForm() {
  const currentStep = removeAccent(getStep());
  switch (currentStep) {
    case removeAccent(TypeStep.AddStaff):
      return configFormAddStaff;
    case removeAccent(TypeStep.WorkProgress):
      return configFormWorkProgress;
    case removeAccent(TypeStep.LaborContract):
      return configFormLaborContract;
    case removeAccent(TypeStep.Salary):
      return configFormSalary;
    default: return []
  }
}

export function clearCurrentForm() {
  return populateData({}, true);
}

export * from "./types";