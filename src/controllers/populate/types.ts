export enum KeyStore {
  KeyCurrentStep = 'keyCurrentStep',
  KeyCurrentIndexAllowance = 'keyCurrentIndexAllowance',
  IsShowPopulate = 'isShowPopulate',
  IsAutoPopulate = 'isAutoPopulate'
}
