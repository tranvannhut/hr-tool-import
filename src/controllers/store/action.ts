import { getStore, setStore } from '.';
import { KeyStore } from '../populate/types';

export function setDataToStore(key: string, data: any) {
  const dataStore = {
    ...getStore(),
    [key]: data
  };
  setStore(dataStore);

  return dataStore;
}

export function getDataFromStore(key: KeyStore) {
  return getStore()[key] || null;
}

export function removeDataFromStore(key: KeyStore | KeyStore[]) {
  ((key.length ? key : [key]) as KeyStore[]).forEach(e => setDataToStore(e, null));
  return null;
}
