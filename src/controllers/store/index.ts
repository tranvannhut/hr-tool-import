const KeyStoreCache = 'store_tool_hris';

export function getStore() {
  return JSON.parse(localStorage.getItem(KeyStoreCache) || '{}');
}

export function setStore(data: any) {
  return localStorage.setItem(KeyStoreCache, JSON.stringify(data));
}

export * from './action';
