import axios from 'axios';
import { keyCacheProfile } from './populate';

const urlLogin = `${process.env.BASE_URL_API}/users/login`;
const urlGetMe = `${process.env.BASE_URL_API}/users/me`;
export const keyToken = 'token';

export function login(username: string, password: string) {
  return axios.post(urlLogin, { email: username, password })
    .then(resp => {
      localStorage.setItem(keyToken, resp.data.token);
      return resp.data;
    })
    .catch(err => console.log(err));
}

export function logout() {
  localStorage.removeItem(keyToken);
  localStorage.removeItem(keyCacheProfile);
}

export function getCurrentUser() {
  return axios.get(urlGetMe, {
    headers: {
      Authorization: localStorage.getItem(keyToken)
    }
  })
    .then(resp => resp.data)
    .catch(err => console.log(err));
}
