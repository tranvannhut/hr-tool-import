import axios, { AxiosRequestConfig } from 'axios';
import { keyToken } from './login';

export function httpClient(config: AxiosRequestConfig) {
  return axios({
    ...config,
    baseURL: process.env.BASE_URL_API,
    headers: {
      Authorization: localStorage.getItem(keyToken)
    }
  });
}
