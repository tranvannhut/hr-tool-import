export enum TypeSync {
  StaffId = 'staff_id',
  DecisionNumber = 'decision_number',
  DoneHris = 'done_hris',
  ContractNo = 'contract_no'
}
