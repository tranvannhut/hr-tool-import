import { TypeSync } from './type';
import { httpClient } from '../../controllers/base-service';

const urlUpdateProfile = `/users/profiles/:id`;
const urlMarkDoneHris = `/users/profiles/:id/mark-done-hris`;

export async function syncData(key, el: JQuery, currentUser) {
  try {
    if (key === TypeSync.DoneHris) {
      if (confirm('Bạn đã hoàn thành nhập dữ liệu Hris ?')) {
        await httpClient({
          url: urlMarkDoneHris.replace(':id', currentUser.id),
          method: 'PUT'
        });

        return alert('Xong!');
      }
      return true;
    }
    const data = getDataSync(key);
    if (!data) { return alert(`Không tồn tại ${el.text()}`); }
    const r = confirm(`Bạn có chắc chắn đồng bộ '${el.text()}' với giá trị '${data}' không?`);
    if (r === true) {
      await httpClient({
        url: urlUpdateProfile.replace(':id', currentUser.id),
        method: 'PUT',
        data: { [getKey(key)]: data }
      });

      return alert('Xong!');
    }
  } catch (error) {
    alert(error.message);
  }

}

function getDataSync(key: TypeSync) {
  switch (key) {
    case TypeSync.StaffId:
      return $('#_ctl0_HR_EmpHeader_txtEmpID').val();
    case TypeSync.DecisionNumber:
      return $('#_ctl0_dtgListctl0 tbody tr:eq(0) td:eq(14)').text();
    case TypeSync.ContractNo:
      return $('#_ctl0_rgrdListctl0 tbody tr:eq(0) td:eq(7)').text();
  }
}

function getKey(key: TypeSync) {
  switch (key) {
    case TypeSync.StaffId:
      return 'staffId';
    case TypeSync.DecisionNumber:
      return 'issuePaperNo';
    case TypeSync.ContractNo:
      return 'contractNo';
  }
}
