import { IForm, TypeInput } from './types';

export const configFormSalary: IForm[] = [
  {
    name: 'Eff date',
    input: '_ctl0:txtFromDate',
    key: 'startDate'
  },
  {
    name: 'Actual Date',
    input: '_ctl0:txtActualDate',
    key: 'startDate'
  },
  {
    name: 'Hệ số gốc',
    input: '_ctl0:txtRootSalary',
    key: 'salaryHrisTotal'
  },
  {
    name: 'Basic salary',
    input: '_ctl0:txtBasicSalary',
    key: 'salaryHrisBase'
  },
  {
    name: 'Loại tiền tệ',
    input: '_ctl0:cboCurrencyRoot',
    key: 'salaryCurrency'
  },
  {
    name: 'Đơn vị tính lương',
    input: '_ctl0:cboDonViTinhLuong',
    type: TypeInput.Dropdown,
    key: 'salaryUnit'
  },
  {
    name: 'Lương Gross/Net',
    key: 'isGrossSalary',
    type: TypeInput.Radio,
    parseValue: (value) => {
      if (value) { return 'Gross'; }
      return 'Net';
    },
    input: [{ input: '_ctl0_rbtGrossNet_0', value: 'Gross' }, { input: '_ctl0_rbtGrossNet_1', value: 'Net' }]
  }
];
