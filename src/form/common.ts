import { TypeInput } from './types';

export const company = {
  name: 'Công ty',
  input: '_ctl0:cboCompanyAllID',
  key: 'company',
  type: TypeInput.Dropdown
};

export const division = {
  name: 'Khối',
  input: '_ctl0:cboLevel1ID',
  type: TypeInput.Dropdown,
  key: 'division'
};

export const center = {
  name: 'Trung tâm',
  input: '_ctl0:cboLevel2ID',
  key: 'center',
  type: TypeInput.Dropdown
};

export const department = {
  name: 'Phòng',
  input: '_ctl0:cboLevel3ID',
  key: 'department',
  type: TypeInput.Dropdown
};

export const unit = {
  name: 'Bộ phận',
  input: '_ctl0:cboLSLevel4IDAll',
  key: 'unit',
  type: TypeInput.Dropdown
};

export const team = {
  name: 'Nhóm',
  input: '_ctl0:cboLSLevel5IDAll',
  key: 'team',
  type: TypeInput.Dropdown
};

export const positionCode = {
  name: 'Chức danh',
  input: '_ctl0:cboLSJobTitleID_Related',
  key: 'positionCode',
  type: TypeInput.Dropdown
};

export const pow = {
  name: 'Nơi Làm việc',
  input: '_ctl0:cboLocationID',
  key: 'pow',
  type: TypeInput.Dropdown
};

export const employeeType = {
  name: 'Loại nhân viên',
  input: '_ctl0:cboLoaiNhanVien',
  key: 'employeeType',
  type: TypeInput.Dropdown
};

export const timeAttendanceGroup = {
  name: 'Nhóm chấm công',
  input: '_ctl0:cboLoaiHinhNhanVien',
  key: 'timeAttendanceGroup',
  type: TypeInput.Dropdown
};

export const changeType = {
  name: 'Hình thức thay đổi',
  input: '_ctl0:cboLSStatusChangeID',
  key: 'changeType',
  type: TypeInput.Dropdown
};
