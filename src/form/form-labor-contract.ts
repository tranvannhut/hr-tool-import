import { IForm, TypeInput } from './types';
import * as moment from 'moment';

export const configFormLaborContract: IForm[] = [
  {
    name: 'Loại hợp đồng',
    input: '_ctl0:cboLSContractTypeID',
    key: 'contractType',
    type: TypeInput.Dropdown
  },
  {
    name: 'Hiệu lực từ ngày',
    input: '_ctl0:txtEffectiveDate',
    key: 'effectiveDate'
  },
  {
    name: 'Đến ngày',
    input: '_ctl0:txtToDate',
    key: 'expiryDate'
  },
  {
    name: 'Thử việc từ ngày',
    input: '_ctl0:txtProbationFrom',
    key: 'dop'
  },
  {
    name: 'Số ngày TV',
    input: '_ctl0:txtSoNgayThuViec',
    key: 'probationPeriod'
  },
  {
    name: 'Thử việc đến ngày',
    input: '_ctl0:txtProbationTo',
    key: 'dop',
    parseValue: (_, current) => {
      if (current.probationPeriod && current.dop) {
        return moment(current.dop, 'DD/MM/YYYY').add(current.probationPeriod, 'days').format('DD/MM/YYYY');
      }

      return null;
    }
  },
  {
    name: 'Người ký',
    input: '_ctl0:HR_EmpSelect1:txtEmpNameS',
    key: 'signerName'
  },
  {
    name: 'Chức danh VN',
    input: '_ctl0:HR_EmpSelect1:txtJobTitle',
    key: 'signerPosition'
  },
  {
    name: 'Theo giấy ủy quyền',
    input: '_ctl0:txtGiayUyQuyen',
    key: 'authLetter'
  },
];
