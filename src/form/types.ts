export interface IForm {
  name: string;
  input: string | IRadio[];
  key?: string;
  type?: TypeInput;
  parseValue?: (value: string, current?: any) => any;
  // config?: IConfig;
}

export interface IConfig {
  radio: IRadio[];
}

export interface IRadio {
  input: string;
  value: string;
}

export enum TypeInput {
  Dropdown = 'dropdown',
  CheckBox = 'checkBox',
  Radio = 'radio'
}
