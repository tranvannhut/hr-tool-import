import { IForm, TypeInput } from './types';

export const configFormAllowance: IForm[] = [
  {
    name: 'Nhóm khoản thêm',
    input: '_ctl0:cboLSAddSalaryGroupID',
    key: 'group',
    type: TypeInput.Dropdown
  },
  {
    name: 'Ngày ký',
    input: '_ctl0:txtFromDate',
    key: 'signDate'
  },
  {
    name: 'Đến ngày',
    input: '_ctl0:txtToDate',
    key: 'expiryDate'
  },
  {
    name: 'Ngày có hiệu lực',
    input: '_ctl0:txtActualDate',
    key: 'effectiveDate'
  },
  {
    name: 'Loại phụ cấp - trợ cấp',
    input: '_ctl0:cboLSAddSalaryID',
    key: 'type',
    type: TypeInput.Dropdown
  },
  {
    name: 'Số tiền',
    input: '_ctl0:txtAmount',
    key: 'value'
  },
  {
    name: 'Loại tiền tệ',
    input: '_ctl0:cboCurrency',
    key: 'currency',
    type: TypeInput.Dropdown
  }
];
