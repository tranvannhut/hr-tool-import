import { IForm, TypeInput } from './types';

export const configFormWorkProgress: IForm[] = [
  {
    name: 'Từ ngày',
    input: '_ctl0:txtFromDate',
    key: 'startDate'
  },
  {
    name: 'Hình thức thay đổi',
    input: '_ctl0:cboLSStatusChangeID',
    key: 'changeType',
    type: TypeInput.Dropdown
  },
  {
    name: 'Có quyết định',
    input: '_ctl0_chkDecision',
    type: TypeInput.CheckBox,
    key: 'paperIssued'
  },
  {
    name: 'Công ty',
    input: '_ctl0:cboCompanyAllID',
    key: 'company',
    type: TypeInput.Dropdown
  },
  {
    name: 'Khối',
    input: '_ctl0:cboLevel1AllID',
    type: TypeInput.Dropdown,
    key: 'division'
  },
  {
    name: 'Trung tâm',
    input: '_ctl0:cboLevel2AllID',
    key: 'center',
    type: TypeInput.Dropdown
  },
  {
    name: 'Phòng',
    input: '_ctl0:cboLevel3AllID',
    key: 'department',
    type: TypeInput.Dropdown
  },
  {
    name: 'Bộ phận',
    input: '_ctl0:cboLSLevel4IDAll',
    key: 'unit',
    type: TypeInput.Dropdown
  },
  {
    name: 'Nhóm',
    input: '_ctl0:cboLSLevel5IDAll',
    key: 'team',
    type: TypeInput.Dropdown
  },
  {
    name: 'Nơi Làm việc',
    input: '_ctl0:cboLocationID',
    key: 'pow',
    type: TypeInput.Dropdown
  },
  {
    name: 'Loại nhân viên',
    input: '_ctl0:cboLoaiNhanVien',
    key: 'employeeType',
    type: TypeInput.Dropdown
  },
  {
    name: 'Nhóm chấm công',
    input: '_ctl0:cboLoaiHinhNhanVien',
    key: 'timeAttendanceGroup',
    type: TypeInput.Dropdown
  },
  {
    name: 'Chức danh',
    input: '_ctl0:cboJobTitle',
    key: 'positionCode',
    type: TypeInput.Dropdown
  }
];
