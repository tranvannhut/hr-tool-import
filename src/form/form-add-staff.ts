import { IForm, TypeInput } from './types';

export const configFormAddStaff: IForm[] = [
  {
    name: 'Công ty',
    input: '_ctl0:cboCompanyID',
    key: 'company',
    type: TypeInput.Dropdown
  },
  {
    name: 'Khối',
    input: '_ctl0:cboLevel1ID',
    type: TypeInput.Dropdown,
    key: 'division'
  },
  {
    name: 'Trung tâm',
    input: '_ctl0:cboLevel2ID',
    key: 'center',
    type: TypeInput.Dropdown
  },
  {
    name: 'Phòng',
    input: '_ctl0:cboLevel3ID',
    key: 'department',
    type: TypeInput.Dropdown
  },
  {
    name: 'Bộ phận',
    input: '_ctl0:cboLSLevel4IDAll',
    key: 'unit',
    type: TypeInput.Dropdown
  },
  {
    name: 'Nhóm',
    input: '_ctl0:cboLSLevel5IDAll',
    key: 'team',
    type: TypeInput.Dropdown
  },
  {
    name: 'Giới tính',
    input: '_ctl0:cboGioiTinh',
    key: 'gender',
    type: TypeInput.Dropdown
  },
  {
    name: 'Tình trạng hôn nhân',
    input: '_ctl0:cboTinhTrangHonNhan',
    key: 'maritalStatus',
    type: TypeInput.Dropdown
  },
  {
    name: 'Quốc tịch',
    input: '_ctl0:cboLSNationalityID',
    type: TypeInput.Dropdown,
    key: 'nationality'
  },
  {
    name: 'Dân tộc',
    input: '_ctl0:cboLSEthnicID',
    type: TypeInput.Dropdown,
    key: 'ethnic'
  },
  {
    name: 'Loại nhân viên',
    input: '_ctl0:cboLoaiNhanVien',
    key: 'employeeType',
    type: TypeInput.Dropdown
  },
  {
    name: 'Chức vụ',
    input: '_ctl0:cboLSChucVuID',
    key: 'positionLevel',
    type: TypeInput.Dropdown
  },
  {
    name: 'Trình độ chuyên môn',
    input: '_ctl0:cboTrinhDoChuyenMon',
    type: TypeInput.Dropdown,
    key: 'qualifications'
  },
  {
    name: 'Hình thức thay đổi',
    input: '_ctl0:cboLSStatusChangeID',
    key: 'changeType',
    type: TypeInput.Dropdown
  },
  {
    name: 'Trình độ học vấn',
    input: '_ctl0:cboTrinhDoHocVan',
    key: 'academicLevel',
    type: TypeInput.Dropdown
  },
  {
    name: 'Nhóm chấm công',
    input: '_ctl0:cboLoaiHinhNhanVien',
    key: 'timeAttendanceGroup',
    type: TypeInput.Dropdown
  },
  {
    name: 'Local/Expat',
    input: '_ctl0:cboNhomNhanVien',
    key: 'localExpat',
    type: TypeInput.Dropdown
  },
  {
    name: 'Chức danh',
    input: '_ctl0:cboLSJobTitleID_Related',
    key: 'positionCode',
    type: TypeInput.Dropdown
  },
  {
    name: 'Nơi Làm việc',
    input: '_ctl0:cboLocationID',
    key: 'pow',
    type: TypeInput.Dropdown
  },

  {
    name: 'Mã nhân viên',
    input: '_ctl0:txtMaNhanVien',
    key: 'staffId'
  },
  {
    name: 'Họ',
    input: '_ctl0:txtHoTenLot',
    key: 'lastName'
  },
  {
    name: 'Tên đệm',
    input: '_ctl0:txtMiddleName',
    key: 'middleName'
  },
  {
    name: 'Tên gọi',
    input: '_ctl0:txtTen',
    key: 'firstName'
  },
  {
    name: 'Nơi sinh (Text)',
    input: '_ctl0:txtNoiSinh'
  },
  {
    name: 'Nơi sinh',
    input: '_ctl0:cboNoiSinh_LSProvinceID',
    key: 'pob'
  },
  {
    name: 'Nơi cấp CMND',
    input: '_ctl0:cboNoiCapCMND',
    key: 'idPoi'
  },
  {
    name: 'Nhập Passport',
    input: '_ctl0:chkPassport'
  },
  {
    name: 'Số passport',
    input: '_ctl0:txtPassportNo'
  },
  {
    name: 'Ngày cấp passport',
    input: '_ctl0:txtNgayCapPassport'
  },
  {
    name: 'Ngày cấp CMND',
    input: '_ctl0:txtNgayCapCMND',
    key: 'idDoi'
  },
  {
    name: 'CMND Số',
    input: '_ctl0:txtSoCMND',
    key: 'idNo'
  },
  {
    name: 'Ngày sinh',
    input: '_ctl0:txtNgaySinh',
    key: 'dob'
  },
  {
    name: 'Ngày hiệu lực',
    input: '_ctl0:txtNgayHieuLucPassport',
    key: 'effectiveDate'
  },
  {
    name: 'Ngày hết hạn',
    input: '_ctl0:txtNgayHetHanPassport',
    key: 'expiryDate'
  },
  {
    name: 'Loại Passport',
    input: '_ctl0:cboLoaiPassport'
  },
  {
    name: 'Nơi cấp Passport',
    input: '_ctl0:txtNoiCapPassport'
  },
  {
    name: 'Nơi cấp',
    input: '_ctl0:cboNoiCapMST'
  },
  {
    name: 'Ngày vào chính thức',
    input: '_ctl0:txtNgayVaoChinhThuc'
  },
  {
    name: 'Số di động',
    input: '_ctl0:txtSoDiDong',
    key: 'phone'
  },
  {
    name: 'Ghi chú',
    input: '_ctl0:txtGhiChuPassport'
  },
  {
    name: 'Ngày vào Công ty',
    input: '_ctl0:txtNgayVaoCongTy',
    key: 'startDate'
  },

  {
    name: 'Cấp trên gián tiếp',
    input: '_ctl0:txtEmpIndirectReport'
  },
  {
    name: 'Số di động',
    input: '_ctl0:txtSoDiDong'
  },
  {
    name: 'Email cá nhân',
    input: '_ctl0:txtEmail',
    key: 'email'
  },
  // {
  //   name: 'Email công ty',
  //   input: '_ctl0:txtCompanyEmail',
  //   // note for UAT
  //   key: 'email'
  // },
  {
    name: 'Mã số thuế',
    input: '_ctl0:txtMaSoThue'
  },
  {
    name: 'Ngày cấp',
    input: '_ctl0:txtNgayCapMST'
  },
  {
    name: 'Nơi cấp',
    input: '_ctl0:cboNoiCapMST'
  },
  {
    name: 'Địa chỉ thường trú',
    input: '_ctl0:txtDiaChiThuongTru',
    key: 'permAddr'
  },
  {
    name: 'Tỉnh / Thành Phố',
    input: '_ctl0:cboP_LSProvinceID',
    key: 'pCity'
  },
  {
    name: 'Quận / Huyện',
    input: '_ctl0:cboP_District',
    key: 'pDistrict'
  },
  {
    name: 'Phường / Xã',
    input: '_ctl0:cboP_Ward',
    key: 'pWard'
  },
  {
    name: 'Địa chỉ tạm trú',
    input: '_ctl0:txtDiaChiTamTru',
    key: 'tempAddr'
  },
  {
    name: 'Tỉnh / Thành Phố',
    input: '_ctl0:cboT_LSProvinceID',
    key: 'tCity'
  },
  {
    name: 'Quận / Huyện',
    input: '_ctl0:cboT_District',
    key: 'tDistrict'
  },
  {
    name: 'Phường / Xã',
    input: '_ctl0:cboT_Ward',
    key: 'tWard'
  },
  {
    name: 'Địa chỉ CMND',
    input: '_ctl0:txtDiaChiTamTruEN'
  },
  {
    name: 'Nơi sinh',
    input: '_ctl0:txt_NoiSinh',
    key: 'pob'
  },
  {
    name: 'Ghi chú',
    input: '_ctl0:txtNoteAdd'
  }
];
