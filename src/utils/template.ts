export function getNamePermission(p: string) {
  switch (p) {
    case 'admin':
      return 'Admin';
    default:
      return 'HR Supporter';
  }
}
