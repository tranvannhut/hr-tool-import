export function removeAccent(i) {
  return i.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}
