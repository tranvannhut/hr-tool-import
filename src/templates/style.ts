export const styles = `
<style>
  .main-tool {
    max-width: 50%;
    margin: 10px;
    border: 1px solid;
    padding: 10px;
  }

  .login-page {
    display:flex;
    justify-content: center;
    margin-top: 5%;
  }

  .login-page input {
    margin-right:5px;
  }

  .header {
    display:flex;
    justify-content: space-between;
  }
  
  .row_t {
    margin: 5px;
  }

  .color_loading {
    opacity: 0.4;
  }

  .flex--justify_sb {
    display:flex;
    justify-content: space-between;
  }
  
  .red-background {
    background-color: red;
    color:white;
  }

  .red-font {
    color: red;
  }
  
  .blue-background {
    background-color: blue;
    color:white;
  }
</style>
`;
