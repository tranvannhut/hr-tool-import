import { getProfiles, KeyStore } from '../controllers';
import { getDataFromStore } from '../controllers/store/action';
import * as lodash from 'lodash';
import { removeAccent } from '../utils';
import { TypeSync } from '../controllers/sync/type';
import { keyCacheSelectedProfile } from '../index';

export const templatePopulate = async () => {
  const profiles = await getProfiles();
  return `
  <div class="populate-page">
    <div class="row_t flex--justify_sb">
      <span><b>Total profiles:</b> <span id="total-profile" class="red-font">${profiles.length}</span></span>
      <button id="btn-logout" class="red-background">Logout</button>
    </div>
    <div class="row_t">
      <b>Current:</b>
      <select id="profiles">
      ${profiles.map(e => `<option value="${e.id}">${
    e.profile ? e.profile.txtTen || e.fullName : e.fullName
    }</option>`)}
      </select>
      <span id="selected-profile">${getSelectedProfileInit(profiles)}</span>
    </div>
    <div class="row_t">
      <span><b>${!getDataFromStore(KeyStore.IsAutoPopulate) ? 'Step' : 'Auto'}:</b> ${getStep()}</span>
      ${!getDataFromStore(KeyStore.IsAutoPopulate) ?
      `<button id="btn-populate" class="blue-background ${checkStatusPopulate() ? undefined : 'color_loading'}"
        ${checkStatusPopulate() ? undefined : 'disabled'}>
        ${checkStatusPopulate() ? 'Populate' : 'Not Support'}
        </button>` : ''}
    </div>
    ${templateTakeData}
    <div class="row_t">
      <span><b>Version:</b> ${process.env.VERSION}</span>
    </div>
  </div>
`;
};

const templateTakeData = `
<div class="row_t">
  <span><b>Sync data</b></span>: 
  <button id="btn-sync_${TypeSync.StaffId}" key="${TypeSync.StaffId}">Mã nhân viên</button> 
  <button id="btn-sync_${TypeSync.DecisionNumber}" key="${TypeSync.DecisionNumber}">Mã số quyết định</button>
  <button id="btn-sync_${TypeSync.ContractNo}" key="${TypeSync.ContractNo}">Mã số hợp đồng</button>
  <button id="btn-sync_${TypeSync.DoneHris}" key="${TypeSync.DoneHris}">Hoàn thành Hris</button>
</div>
`;

export function getStep(): any {
  return $('#lblTitle').text();
}

export function checkStatusPopulate() {
  if ([TypeStep.AddStaff, TypeStep.WorkProgress, TypeStep.LaborContract, TypeStep.Salary, TypeStep.Allowance]
    .map(removeAccent)
    .includes(removeAccent(getStep()))) {
    return true;
  }
  return false;
}

export enum TypeStep {
  AddStaff = 'THÊM MỚI NHÂN VIÊN',
  WorkProgress = 'QÚA TRÌNH LÀM VIỆC',
  LaborContract = 'HỢP ĐỒNG LAO ĐỘNG',
  Salary = 'NHẬP LƯƠNG CHO HỢP ĐỒNG',
  Allowance = 'PHỤ CẤP - TRỢ CẤP'
}

export function getSelectedProfileInit(profiles) {
  const selected = localStorage.getItem(keyCacheSelectedProfile);
  let profile = lodash.get(profiles[0], 'email', '');
  if (selected) {
    profile = profiles.find(e => e.id === selected);
  }
  return lodash.get(profile, 'email', '');
}
